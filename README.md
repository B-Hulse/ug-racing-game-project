# L3Project_2

Developed with Unreal Engine 4

Demo video available [here](https://www.youtube.com/watch?v=QoL6AVoDX04).

## Playing the final build

The playable final build of the game can be found in the folder 'FinalBuild' as an executable file for Windows 64Bit. Simply Run the L3Project_2.exe file to play the game.
The Controls of the game are as follows:

### Keyboard/Mouse Controls:
* W - Accelerate Vehicle
* S - Brake/Reverse Vehicle
* A/D - Turn Vehicle
* Space Bar - Handbrake
* Enter - Accept (In Start/End Screens)
* Backspace - Reset Vehicle
* C - Reverse Cam
* Tab - Toggle In car camera
* Left/Right - Skycam prev/next target
* Mouse movement - Look around (In-car view or skycam free-look mode)
* Mouse Scroll Wheel - Adjust skycam zoom

### Joypad Controls:
* Right Trigger - Accelerate Vehicle
* Left Trigger - Brake/Reverse Vehicle
* Left Analog - Turn Vehicle
* A Button - Handbrake
* Start - Accept (In Start/End Screens)
* Y Button - Reset Vehicle
* Left Analog In - Reverse Cam
* Select - Toggle In car camera
* D-Pad Left/Right - Skycam prev/next target
* Right Analog - Look around (In-car view or skycam free-look mode)
* D-Pad Up/Down - Adjust skycam zoom
    
The game must be exited using Alt-F4.
